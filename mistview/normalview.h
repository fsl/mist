/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef NORMALVIEW_H
#define NORMALVIEW_H

#include "viewbase.h"

class vtkRenderWindowInteractor;
class vtkOrientationMarkerWidgetFixed;

class NormalView : public ViewBase
{
public:
    NormalView(vtkSmartPointer<vtkCamera> camera, vtkSmartPointer<vtkRenderWindow> renderwindow);
    vtkSmartPointer<vtkRenderWindow> GetView();
    vtkIdType Pick(const double& x, const double& y);
//    void SetupOrientationWidget(vtkSmartPointer<vtkRenderWindowInteractor> interactor);

protected:
//    vtkSmartPointer<vtkOrientationMarkerWidgetFixed> m_orientationWidget;

    virtual void ContentsChanged();
    virtual void SetupSceneForView(View& vw);
    virtual void SetupCameraForView(View& vw);
//    virtual void SetupMarkersForView(View &vw);
};

#endif // NORMALVIEW_H
