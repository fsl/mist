/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef CUSTOMINTERACTORSTYLE_H
#define CUSTOMINTERACTORSTYLE_H

#include <memory>
#include <vtkType.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSetGet.h>
#include <functional>
#include "newmat.h"

class OrthoViews;

class CustomInteractorStyle : public vtkInteractorStyleTrackballCamera
{
public:
    enum OrthoMode
    {
        Sagittal,
        Coronal,
        Transverse
    };

    static CustomInteractorStyle* New();
    vtkTypeMacro(CustomInteractorStyle, vtkInteractorStyleTrackballCamera);

    void SetOrthoViews(std::shared_ptr<OrthoViews> orthoviews);
    void SetOrthoMode(OrthoMode orthomode);
    void SetPickerFunc(std::function<void(const double& x, const double& y)> pickerfunc);
    void SetOriginFunc(std::function<void(const NEWMAT::ColumnVector& origin)> originfunc);

protected:
    std::function<void(const double&x, const double& y)> m_pickerFunc = nullptr;
    std::function<void(const NEWMAT::ColumnVector& origin)> m_originFunc = nullptr;

    OrthoMode m_orthoMode = Sagittal;
    std::shared_ptr<OrthoViews> m_orthoViews = nullptr;

    virtual void OnLeftButtonDown();
    virtual void OnRightButtonDown();
};

#endif // CUSTOMINTERACTORSTYLE_H
