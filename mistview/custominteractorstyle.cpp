/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "custominteractorstyle.h"
#include "orthoviews.h"
#include <vtkObjectFactory.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCoordinate.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>

#include <newmat.h>

#include <iostream>

using namespace NEWMAT;

vtkStandardNewMacro(CustomInteractorStyle);

void CustomInteractorStyle::SetOrthoViews(std::shared_ptr<OrthoViews> orthoviews)
{
    m_orthoViews = orthoviews;
}

void CustomInteractorStyle::SetOrthoMode(OrthoMode orthomode)
{
    m_orthoMode = orthomode;
}

void CustomInteractorStyle::SetPickerFunc(std::function<void(const double& x, const double& y)> pickerfunc)
{
    m_pickerFunc = pickerfunc;
}

void CustomInteractorStyle::SetOriginFunc(std::function<void (const ColumnVector &)> originfunc)
{
    m_originFunc = originfunc;
}

void CustomInteractorStyle::OnLeftButtonDown()
{
    if (m_orthoViews && m_originFunc)
    {
        int x, y;
        GetInteractor()->GetEventPosition(x, y);
        auto converter = vtkSmartPointer<vtkCoordinate>::New();
        converter->SetCoordinateSystemToDisplay();
        converter->SetViewport(GetCurrentRenderer());
        converter->SetValue(x, y);
        ColumnVector world(3);
        world << converter->GetComputedWorldValue(GetDefaultRenderer());
        switch (m_orthoMode)
        {
            case Sagittal:
                world(1) = m_orthoViews->GetOrigin()(1);
                break;
            case Coronal:
                world(2) = m_orthoViews->GetOrigin()(2);
                break;
            case Transverse:
                world(3) = m_orthoViews->GetOrigin()(3);
                break;
        }

        m_originFunc(world);
    }
}

void CustomInteractorStyle::OnRightButtonDown()
{
    if (m_pickerFunc)
    {
        int x, y;
        GetInteractor()->GetEventPosition(x, y);
        m_pickerFunc(x, y);
    }
}
