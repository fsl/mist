/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "viewdata.h"
#include <vtkLookupTable.h>

vtkSmartPointer<vtkImageData> ViewData::GetImageData()
{
    return m_imageData;
}

std::string ViewData::GetDisplayName() const
{
    return m_displayName;
}

double ViewData::GetDisplayMin() const
{
    return m_lut->GetTableRange()[0];
}

double ViewData::GetDisplayMax() const
{
    return m_lut->GetTableRange()[1];
}

void ViewData::SetDisplayMin(const double &min)
{
    m_lut->SetTableRange(min, GetDisplayMax());
}

void ViewData::SetDisplayMax(const double &max)
{
    m_lut->SetTableRange(GetDisplayMin(), max);
}

vtkSmartPointer<vtkLookupTable> ViewData::GetLut()
{
    return m_lut;
}

void ViewData::SetBinaryLut(double hue, double saturation)
{
    m_lut = BuildBinaryLut(hue, saturation);
    SetDisplayMin(0.0);
    SetDisplayMax(1.0);
}

vtkSmartPointer<vtkLookupTable> ViewData::BuildLut(double min, double max)
{
    auto lut = vtkSmartPointer<vtkLookupTable>::New();
    lut->SetRampToLinear();
    lut->SetTableRange(min, max);
    lut->SetHueRange(0.0, 0.0);
    lut->SetSaturationRange(0.0, 0.0);
    lut->SetValueRange(0.0, 1.0);
    lut->SetNanColor(0.0, 0.0, 0.0, 0.0);
    lut->Build();

    return lut;
}

vtkSmartPointer<vtkLookupTable> ViewData::BuildBinaryLut(double hue, double saturation)
{
    auto lut = vtkSmartPointer<vtkLookupTable>::New();
    lut->SetRampToLinear();
    lut->SetTableRange(0.0, 1.0);
    lut->SetHueRange(hue, hue);
    lut->SetSaturationRange(saturation, saturation);
    lut->SetValueRange(1.0, 1.0);
    lut->SetAlphaRange(0.0, 1.0);
    lut->SetNanColor(0.0, 0.0, 0.0, 0.0);
    lut->Build();
 
    return lut;
}

