/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "mainwindow.h"
#include <QApplication>

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/support/date_time.hpp>

#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL)
VTK_MODULE_INIT(vtkRenderingContextOpenGL)
VTK_MODULE_INIT(vtkInteractionStyle)
VTK_MODULE_INIT(vtkRenderingFreeType)

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("FMRIB Centre");
    QCoreApplication::setOrganizationDomain("fmrib.ox.ac.uk");
    QCoreApplication::setApplicationName("mistview");

    boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::info);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
