/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef VIEWSHAPE_H
#define VIEWSHAPE_H

#include "shape.h"
#include <array>

class ViewShape : public Shape
{
public:
    ViewShape(vtkSmartPointer<vtkPolyData> polydata, const std::string& displayname);
    ViewShape(const std::string& filename, const std::string& displayname);
    ViewShape(const std::string& filename, boost::shared_ptr<const Transformation> &transformation,
              const std::string& displayname);

    ViewShape(const ViewShape &other);
    ViewShape(const ViewShape &other, boost::shared_ptr<const Transformation> &transformation);
    ViewShape(const Shape &other);
    ViewShape& operator=(const ViewShape &other);

    void SetColor(const std::array<double, 3>& rgb);
    void SetColor(const double& r, const double& g, const double& b);
    std::array<double, 3> GetColor() const;
    void SetLineWidth(float width);
    float GetLineWidth() const;
    void SetOpacity(double opacity);
    double GetOpacity() const;

protected:
    void Copy(const ViewShape &other);

private:
    std::array<double, 3> m_rgb = {{1.0, 1.0, 1.0}};
    float m_lineWidth = 1.0;
    double m_opacity = 1.0;
};

#endif // VIEWSHAPE_H
