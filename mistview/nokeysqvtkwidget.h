/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef NOKEYSQVTKWIDGET_H
#define NOKEYSQVTKWIDGET_H

#include "QVTKWidget.h"

class NoKeysQVTKWidget : public QVTKWidget
{
    Q_OBJECT

public:
    using QVTKWidget::QVTKWidget;

    virtual void keyPressEvent(QKeyEvent *event);
};

#endif // NOKEYSQVTKWIDGET_H
