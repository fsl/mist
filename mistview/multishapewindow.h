/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef MULTISHAPEWINDOW_H
#define MULTISHAPEWINDOW_H

#include "transformation.h"
#include "viewdata.h"
#include "orthoviews.h"
#include "nokeysqvtkwidget.h"
#include <QDialog>
#include <memory>
#include <vector>

class Transformation;

namespace Ui {
class MultiShapeWindow;
}

class MultiShapeWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MultiShapeWindow(QWidget *parent, const std::vector<std::shared_ptr<ViewData> > &volumes,
                              boost::shared_ptr<const Transformation> transformation, const std::string &directory);
    ~MultiShapeWindow();

private slots:
    void on_sagittalSlider_valueChanged(int position);
    void on_coronalSlider_valueChanged(int position);
    void on_transverseSlider_valueChanged(int position);
    void on_modalityComboBox_currentIndexChanged(int index);

    void on_hideOverlaysComboBox_stateChanged(int value);

private:
    const int m_sliderSubDivision = 10;
    const int m_minimumViewWidth = 250;
    const int m_minimumViewHeight = 250;

    Ui::MultiShapeWindow *ui;

    std::vector<std::shared_ptr<ViewData> > m_volumes;

    std::shared_ptr<OrthoViews> m_orthoViews;

    NoKeysQVTKWidget *m_sagittalWidget;
    NoKeysQVTKWidget *m_coronalWidget;
    NoKeysQVTKWidget *m_transverseWidget;

    void SetupInterface();
    void SetOrigin(const NEWMAT::ColumnVector& origin);
    void SelectVolume(int index);
};

#endif // MULTISHAPEWINDOW_H
