/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef PLOTWINDOW_H
#define PLOTWINDOW_H

#include "serialisation.h"
#include "mvnshapemodel.h"
#include <boost/shared_ptr.hpp>
#include <QDialog>
#include <vector>
#include <vtkSmartPointer.h>

class vtkContextView;

namespace Ui {
class PlotWindow;
}

class PlotWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PlotWindow(QWidget *parent = 0);
    ~PlotWindow();

    void SetModel(boost::shared_ptr<const ShapeModel> model);
    void SetVolume(std::string name, boost::shared_ptr<NEWIMAGE::volume<double> > vol);
    void SetNormExclusionVolume(boost::shared_ptr<NEWIMAGE::volume<double> > vol);
    void SetTransformation(boost::shared_ptr<const Transformation> &transformation);

    void ShowVertex(int vertex, double displacement);

    void Clear();

protected:
    virtual void showEvent(QShowEvent *event);
    virtual void hideEvent(QHideEvent *event);

private:
    boost::shared_ptr<const ShapeModel> m_model;
    std::vector<std::vector<vtkSmartPointer<vtkContextView> > > m_contextViews;
    std::unordered_map<std::string, boost::shared_ptr<NEWIMAGE::volume<double> > > m_volumes;
    boost::shared_ptr<NEWIMAGE::volume<double> > m_normExclusionVolume;
    boost::shared_ptr<const Transformation> m_transformation;

    Ui::PlotWindow *ui;
};

#endif // PLOTWINDOW_H
