/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QVTKWidget.h>
#include "transformation.h"
#include "orthoviews.h"
#include "normalview.h"
#include <vtkType.h>
#include <memory>
#include <string>
#include <vector>
#include "plotwindow.h"
#include "newmat.h"

class NoKeysQVTKWidget;
class vtkOrientationMarkerWidget;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void PickSagittal(double x, double y);
    void PickCoronal(double x, double y);
    void PickTransverse(double x, double y);
    void PickNormal(const std::shared_ptr<NormalView>& normalView, double x, double y);

    void SetOrigin(const NEWMAT::ColumnVector& origin);
    void SelectVertex(vtkIdType vertex);
    void SelectVolume(int index);

    static std::vector<double> ReadDisplacements(std::string filename, int vertices);

private slots:
    void on_sagittalSlider_valueChanged(int position);
    void on_coronalSlider_valueChanged(int position);
    void on_transverseSlider_valueChanged(int position);
    void on_vertexIdSpinBox_valueChanged(int value);
    void on_modalityComboBox_currentIndexChanged(int index);
    void on_adjustMmSpinBox_valueChanged(double value);
    void on_xMmSpinBox_valueChanged(double value);
    void on_yMmSpinBox_valueChanged(double value);
    void on_zMmSpinBox_valueChanged(double value);
    void on_windowMinSpinBox_valueChanged(double value);
    void on_windowMaxSpinBox_valueChanged(double value);
    void on_openAction_triggered();
    void on_closeAction_triggered();
    void on_saveAction_triggered();
    void on_loadAction_triggered();
    void on_showProfileFitAction_triggered();
    void on_showMultiShapeAction_triggered();
    void on_saveSlicesPNGAction_triggered();
    void on_setupFilenamesAction_triggered();
    void on_quitAction_triggered();

    void on_slabThicknessSpinBox_valueChanged(double value);

    void on_hideOverlaysCheckBox_stateChanged(int value);

private:
    struct Modality
    {
        std::string name;
        std::string filename;
    };

    Ui::MainWindow *ui;

    const int m_sliderSubDivision = 10;
    const int m_minimumViewWidth = 250;
    const int m_minimumViewHeight = 250;

    std::shared_ptr<OrthoViews> m_orthoViews;
    std::vector<std::shared_ptr<NormalView> > m_normalViews;
    std::vector<std::unique_ptr<NoKeysQVTKWidget> > m_normalViewWidgets;

    boost::shared_ptr<ShapeModel> m_model;

    std::vector<std::shared_ptr<ViewData> > m_volumes;
    std::shared_ptr<ViewShape> m_shape;
    std::shared_ptr<ViewShape> m_extraShape;
    std::shared_ptr<ViewData> m_overlay;
    boost::shared_ptr<const Transformation> m_transformation;

    std::vector<Modality> m_volumeFilenames;
    std::string m_modelFilename;
    std::string m_transformFilename;
    std::string m_displacementsBasename;
    std::string m_normExclusionFilename;
    std::string m_outputFilename;
    std::string m_extraShapeFilename;
    std::string m_overlayFilename;

    QLabel m_statusLabel;
    bool m_unsavedChanges = false;
    std::string m_selectedDirectory;

    PlotWindow m_plotWindow;

    virtual void keyPressEvent(QKeyEvent* event);

    void NormalViewInteractionEventHandler(vtkObject *caller, unsigned long eventId, void *callData);

    void LoadVolume(std::string filename, std::string name);
    void CloseSubject();
    void WriteDisplacements(std::string filename);
    bool SaveInteractive();
    void LoadInteractive();

    void GetFilenames(bool showdialog);

    void SetupInterface();
    void EnableInterface(bool enable);
    void CreateNormalViews();
    void DestroyNormalViews();
};

#endif // MAINWINDOW_H
