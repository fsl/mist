/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "nokeysqvtkwidget.h"
#include <vtkRenderWindow.h>
#include <QVTKInteractorAdapter.h>
#include <QResizeEvent>

void NoKeysQVTKWidget::keyPressEvent(QKeyEvent *event)
{
    QWidget::keyPressEvent(event);
}
