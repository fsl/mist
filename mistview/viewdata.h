/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef VIEWDATA_H
#define VIEWDATA_H

#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <newimage.h>
#include <string>
#include <boost/shared_ptr.hpp>

class vtkLookupTable;

class ViewData
{
public:
    template <class T> ViewData(boost::shared_ptr<NEWIMAGE::volume<T> > vol, const std::string& displayname) :
        m_displayName(displayname)
    {
        m_imageData = NewimageToVtk(*vol);
    }

    vtkSmartPointer<vtkImageData> GetImageData();
    std::string GetDisplayName() const;
    double GetDisplayMin() const;
    double GetDisplayMax() const;
    void SetDisplayMin(const double& min);
    void SetDisplayMax(const double& max);
    vtkSmartPointer<vtkLookupTable> GetLut();
    void SetBinaryLut(double hue, double saturation);

private:
    vtkSmartPointer<vtkImageData> m_imageData;
    std::string m_displayName;
    vtkSmartPointer<vtkLookupTable> m_lut;

    static vtkSmartPointer<vtkLookupTable> BuildLut(double min, double max);
    static vtkSmartPointer<vtkLookupTable> BuildBinaryLut(double hue, double saturation);

    template <class T> vtkSmartPointer<vtkImageData> NewimageToVtk(const NEWIMAGE::volume<T>& vol)
    {
        vtkSmartPointer<vtkFloatArray> values = vtkSmartPointer<vtkFloatArray>::New();

        values->SetNumberOfComponents(1);
        for (int k = vol.minz(); k <= vol.maxz(); k++)
            for (int j = vol.miny(); j <= vol.maxy(); j++)
                for (int i = vol.minx(); i <= vol.maxx(); i++)
                   values->InsertNextTuple1(static_cast<float>(vol(i, j, k)));

        vtkSmartPointer<vtkImageData> imagedata = vtkSmartPointer<vtkImageData>::New();
        imagedata->SetExtent(0, vol.xsize() - 1, 0, vol.ysize() - 1, 0, vol.zsize() - 1);
        imagedata->SetSpacing(vol.xdim(), vol.ydim(), vol.zdim());
        imagedata->GetPointData()->SetScalars(values);

        m_lut = BuildLut(vol.robustmin(), vol.robustmax());

        return imagedata;
    }
};

#endif // VIEWDATA_H
