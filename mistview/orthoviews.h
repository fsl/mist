/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef ORTHOVIEWS_H
#define ORTHOVIEWS_H

#include "viewbase.h"
#include <memory>

class vtkActor;
class vtkCursor3D;

class OrthoViews : public ViewBase
{
public:
    OrthoViews(vtkSmartPointer<vtkRenderWindow> sagittal, vtkSmartPointer<vtkRenderWindow> coronal,
               vtkSmartPointer<vtkRenderWindow> transverse);

    NEWMAT::ColumnVector GetOrigin() const;
    void SetOrigin(const NEWMAT::ColumnVector& origin);
    NEWMAT::ColumnVector GetBounds() const;

    vtkSmartPointer<vtkRenderWindow> GetSagittalView() const;
    vtkSmartPointer<vtkRenderWindow> GetCoronalView() const;
    vtkSmartPointer<vtkRenderWindow> GetTransverseView() const;

    vtkIdType PickSagittal(const double& x, const double& y);
    vtkIdType PickCoronal(const double& x, const double& y);
    vtkIdType PickTransverse(const double& x, const double& y);

protected:
    NEWMAT::ColumnVector m_origin;

    virtual void ContentsChanged();
    virtual void MarkerPointIDChanged();
    virtual void SetupSceneForView(View& vw);

    void OriginChanged();

private:
    vtkSmartPointer<vtkCursor3D> m_cursor;
    vtkSmartPointer<vtkActor> BuildCursor(const NEWMAT::ColumnVector& focalpoint);
};

#endif // ORTHOVIEWS_H
