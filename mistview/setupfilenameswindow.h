/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef SETUPFILENAMESWINDOW_H
#define SETUPFILENAMESWINDOW_H

#include <QDialog>
#include <string>
#include <map>

namespace Ui {
class SetupFilenamesWindow;
}

class SetupFilenamesWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit SetupFilenamesWindow(QWidget *parent = 0);
    ~SetupFilenamesWindow();

    std::string LoadConfigurations();
    void SaveConfigurations(const std::string &selected);
    void ShowConfiguration(const std::string &name);
    void EmptyConfiguration();

    int GetNumberOfVolumes() const;
    void GetVolumeFilename(int i, std::string& name, std::string& filename);
    std::string GetModelFilename() const;
    std::string GetTransformFilename() const;
    std::string GetDisplacementsBasename() const;
    std::string GetNormExclusionFilename() const;
    std::string GetOutputFilename() const;
    std::string GetExtraShapeFilename() const;
    std::string GetOverlayFilename() const;

    int GetNumberOfShapes() const;
    void GetShape(int i, std::string& filename, std::string &displacementsbasename, float &red, float &green, float &blue) const;

public slots:
    virtual void accept();

private slots:
    void on_configurationComboBox_currentIndexChanged(const QString &text);
    void on_addButton_clicked();
    void on_removeButton_clicked();
    void on_deleteButton_clicked();
    void on_multiShapeAddButton_clicked();
    void on_multiShapeRemoveButton_clicked();

private:
    struct Configuration
    {
        std::string modelFilename;
        std::string transformFilename;
        std::string displacementsBasename;
        std::string normExclusionFilename;
        std::string outputFilename;
        std::string extraShapeFilename;
        std::string overlayFilename;
        std::map<std::string, std::string> volumeFilenames;
    };

    std::map<std::string, Configuration> m_configurations;

    Ui::SetupFilenamesWindow *ui;
};

#endif // SETUPFILENAMESWINDOW_H
