/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "viewshape.h"
#include <vtkPolyData.h>

using namespace NEWMAT;

ViewShape::ViewShape(vtkSmartPointer<vtkPolyData> polydata, const std::string& displayname)
    : Shape::Shape(polydata, displayname)
{
}

ViewShape::ViewShape(const std::string& filename, const std::string& displayname)
    : Shape(filename, displayname)
{
}

ViewShape::ViewShape(const std::string& filename, boost::shared_ptr<const Transformation> &transformation,
                     const std::string& displayname)
    : Shape(filename, transformation, displayname)
{
}

ViewShape::ViewShape(const ViewShape &other) :
    Shape(other)
{
    // TODO: Clean up - right now the base class members are compied twice
    Copy(other);
}

ViewShape::ViewShape(const ViewShape &other, boost::shared_ptr<const Transformation> &transformation)
    : Shape(other, transformation)
{
    m_lineWidth = other.m_lineWidth;
    m_opacity = other.m_opacity;
    m_rgb = other.m_rgb;
}

ViewShape::ViewShape(const Shape &other)
    : Shape(other)
{

}


ViewShape& ViewShape::operator=(const ViewShape &other)
{
    // Not sure if VTK's deep copy will accept copying from self
    if (&other != this)
        Copy(other);

    return *this;
}

void ViewShape::Copy(const ViewShape &other)
{
    Shape::Copy(other);
    m_lineWidth = other.m_lineWidth;
    m_opacity = other.m_opacity;
    m_rgb = other.m_rgb;
}

void ViewShape::SetColor(const std::array<double, 3>& rgb)
{
    m_rgb = rgb;
}

void ViewShape::SetColor(const double& r, const double& g, const double& b)
{
    m_rgb[0] = r;
    m_rgb[1] = g;
    m_rgb[2] = b;
}

std::array<double, 3> ViewShape::GetColor() const
{
    return m_rgb;
}

void ViewShape::SetLineWidth(float width)
{
    m_lineWidth = width;
}

float ViewShape::GetLineWidth() const
{
    return m_lineWidth;
}

void ViewShape::SetOpacity(double opacity)
{
    m_opacity = opacity;
}

double ViewShape::GetOpacity() const
{
    return m_opacity;
}
