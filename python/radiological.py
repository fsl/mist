#   Copyright (C) 2016 University of Oxford
#   SHBASECOPYRIGHT
import nibabel.nifti1 as nii
import numpy as np
def load(filename):
    vol = nii.load(filename)

    if np.linalg.det(vol.affine) > 0.0:
        imgdata = vol.get_fdata()[::-1, ...]
        flipmat = np.array(
                [[-1.0, 0.0, 0.0, imgdata.shape[0] - 1.0],
                 [0.0, 1.0, 0.0, 0.0],
                 [0.0, 0.0, 1.0, 0.0],
                 [0.0, 0.0, 0.0, 1.0]])
        affine = vol.affine.dot(flipmat)
    else:
        imgdata = vol.get_fdata()
        affine = vol.affine

    rvol = nii.Nifti1Image(imgdata, affine)
    rvol.set_qform(affine)

    return rvol
