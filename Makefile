include ${FSLCONFDIR}/default.mk

PROJNAME  = mist
XFILES    = mist/mist
SCRIPTS   = bin/mist_1_train \
            bin/mist_2_fit \
            bin/mist_FA_reg \
            bin/mist_display \
            bin/mist_mesh_utils
PYFILES   = python/*
DATAFILES = data/masks data/meshes
VTKSUFFIX = -9.3

USRINCFLAGS = -DBOOST_LOG_DYN_LINK \
              -Icommon \
              -I${FSLDIR}/include/vtk${VTKSUFFIX}

OBJS = mist/mist.o $(patsubst %.cpp,%.o,$(wildcard common/*.cpp))

UNAME := $(shell uname)
ifneq (${UNAME},Darwin)
        LIBRT = -lrt
endif

LIBS = -lvtkIOLegacy${VTKSUFFIX} \
       -lvtkIOCore${VTKSUFFIX} \
       -lvtkFiltersModeling${VTKSUFFIX} \
       -lvtkFiltersCore${VTKSUFFIX} \
       -lvtkCommonExecutionModel${VTKSUFFIX} \
       -lvtkCommonDataModel${VTKSUFFIX} \
       -lvtkCommonMisc${VTKSUFFIX} \
       -lvtkCommonSystem${VTKSUFFIX} \
       -lvtkCommonTransforms${VTKSUFFIX} \
       -lvtkCommonMath${VTKSUFFIX} \
       -lvtkCommonCore${VTKSUFFIX} \
       -lvtksys${VTKSUFFIX} \
       -lfsl-warpfns -lfsl-basisfield -lfsl-meshclass -lfsl-newimage \
       -lfsl-miscmaths -lfsl-NewNifti -lfsl-cprob -lfsl-utils -lfsl-znz \
       -lboost_log -lboost_log_setup -lboost_thread \
       -lboost_filesystem -lboost_date_time -lboost_chrono \
       -lboost_serialization -lboost_regex -lboost_system \
       -lnlopt -lgdc -lgd -lpng -lsqlite3 -lpthread -ldl ${LIBRT}

all: ${XFILES}

mist/mist: ${OBJS}
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

clean:
	rm -f ${OBJS} mist/mist
