/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "profilefilters.h"

BOOST_CLASS_EXPORT_IMPLEMENT(DerivativeFilter)
BOOST_CLASS_EXPORT_IMPLEMENT(IdentityFilter)

NEWMAT::ColumnVector IdentityFilter::Filter(const NEWMAT::ColumnVector &profile)
{
    return profile;
}

DerivativeFilter::DerivativeFilter()
{
}

DerivativeFilter::DerivativeFilter(Mode mode) :
    m_mode(mode)
{
}

void DerivativeFilter::SetMode(Mode mode)
{
    m_mode = mode;
}

DerivativeFilter::Mode DerivativeFilter::GetMode() const
{
    return m_mode;
}

NEWMAT::ColumnVector DerivativeFilter::Filter(const NEWMAT::ColumnVector &profile)
{
    NEWMAT::ColumnVector derivative(profile.Nrows());
    derivative = 0.0;

    for (std::size_t i = 2; i <= profile.Nrows() - 1; i++)
        derivative(i) = (profile(i + 1) - profile(i - 1)) / 2.0;

    if (m_mode == Positive)
    {
        BOOST_LOG_TRIVIAL(debug) << "DerivativeFilter: Filtering profile (Positive mode)";

        for (std::size_t i = 1; i <= profile.Nrows(); i++)
            if (derivative(i) < 0)
                derivative(i) = 0;
    }
    else if (m_mode == Negative)
    {
        BOOST_LOG_TRIVIAL(debug) << "DerivativeFilter: Filtering profile (Negative mode)";

        for (std::size_t i = 1; i <= profile.Nrows(); i++)
            if (derivative(i) > 0)
                derivative(i) = 0;
    }
    else
        BOOST_LOG_TRIVIAL(debug) << "DerivativeFilter: Filtering profile (Full mode)";

    return derivative;
}
