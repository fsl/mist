/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef PLOTTING_H
#define PLOTTING_H

#include "armawrap/newmat.h"
#include "sqlite3.h"
#include <vector>
#include <string>
#include <stdexcept>
#include <functional>
#include <boost/shared_ptr.hpp>

namespace Plotting
{
    typedef std::function<void(const std::vector<NEWMAT::ColumnVector>&, const std::vector<std::size_t>&,
                               int, int, const std::string&)> plotfunc;

    class PlottingException : public std::logic_error
    {
    public:
        PlottingException(const std::string &what) : logic_error(what) { }
    };

    void plotpng(const std::vector<NEWMAT::ColumnVector> &data, const std::vector<std::size_t> &deltas, int steps,
                 int vertex, const std::string &plotname, const std::string &basename);

    void plotsqlite(const std::vector<NEWMAT::ColumnVector> &data, const std::vector<std::size_t> &deltas, int steps,
                    int vertex, const std::string &plotname, const std::string &dbfile, int jobid);
}

#endif // PLOTTING_H
