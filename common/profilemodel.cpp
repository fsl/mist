/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "profilemodel.h"
#include <boost/make_shared.hpp>
#include <boost/log/trivial.hpp>

ProfileModel::ProfileModel(const std::vector<std::string> &modalitynames, int reflength, int datalength)
    : m_modalityNames(modalitynames),
      m_refLength(reflength),
      m_dataLength(datalength)
{
    BOOST_LOG_TRIVIAL(debug) << "Initialising base ProfileModel";

    for (auto mn : modalitynames)
    {
        m_globalMean[mn] = 0.0;
        m_globalStdev[mn] = 1.0;
    }
}

ProfileModel::~ProfileModel()
{
}

int ProfileModel::GetNumberOfSteps() const
{
    return m_refLength - m_dataLength;
}

void ProfileModel::SetNormalisation(const std::string &modality, double mean, double stdev)
{
    if (m_globalMean.find(modality) == m_globalMean.end())
        throw ModelException("Specified modality does not exist in model");

    m_globalMean[modality] = mean;

    // This scaling constant changes the scaling between the mean and covcoefs parts of the optimization parameters
    // as covcoefs is a squared quantity .. it is arbitrary, but helps a lot for numerical stability
    BOOST_LOG_TRIVIAL(debug) << "Note: Scaling intensities by 1000 for optimisation";
    m_globalStdev[modality] = 0.001 * stdev;
}
