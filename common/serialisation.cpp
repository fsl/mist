/*  Multimodal Image Segmentation Tool (MIST)  */
/*  Eelke Visser  */

/*  Copyright (c) 2016 University of Oxford  */

/*  CCOPYRIGHT  */

#include "serialisation.h"

BOOST_CLASS_EXPORT_IMPLEMENT(NEWMAT::Matrix);
BOOST_CLASS_EXPORT_IMPLEMENT(NEWMAT::SymmetricMatrix);
BOOST_CLASS_EXPORT_IMPLEMENT(NEWMAT::DiagonalMatrix);
BOOST_CLASS_EXPORT_IMPLEMENT(NEWMAT::ColumnVector);
BOOST_CLASS_EXPORT_IMPLEMENT(NEWMAT::RowVector);

